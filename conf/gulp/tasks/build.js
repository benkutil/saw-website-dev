var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('build', ['clean'], function (done) {
  runSequence('templates', 'styles', 'styles:optimize', ['scripts', 'fonts', 'images'], 'assets:inline', 'assets:useref', 'rev', done);
});
