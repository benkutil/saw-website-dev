var gulp = require('gulp');
var gif = require('gulp-if');
var size = require('gulp-size');
var uglify = require('gulp-uglify');
var useref = require('gulp-useref');
var inline = require('gulp-inline-source');
var path = require('path');


var INLINE_OPTIONS = {
    rootpath: path.resolve('./')
};

gulp.task('assets', ['assets:useref'], function () {

});


gulp.task('assets:useref' ,function () {
  var assets = useref.assets({searchPath: ['.tmp', './']});

  return gulp.src('.tmp/**/*.html')
    .pipe(assets)
    // Concatenate And Minify JavaScript
    .pipe(gif('*.js', uglify({preserveComments: 'some'})))
    // Remove Any Unused CSS
    // Note: If not using the Style Guide, you can delete it from
    // the next line to only include styles your project uses.
    .pipe(assets.restore())
    .pipe(useref())
    // Update Production Style Guide Paths
    //.pipe($.replace('components/components.css', 'components/main.min.css'))
    // Minify Any HTML
    // Output Files
    .pipe(gulp.dest('.tmp'))
    .pipe(size({title: 'html', gzip: true}));
});


gulp.task('assets:inline', function () {
    return gulp.src('.tmp/*.html')
        .pipe(inline(INLINE_OPTIONS))
        .pipe(gulp.dest('./.tmp'));
});
