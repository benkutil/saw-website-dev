var gulp = require('gulp');
var filter = require('gulp-filter');
var flatten = require('gulp-flatten');

gulp.task('fonts', function () {
  return gulp.src(require('main-bower-files')().concat('src/assets/fonts/**/*'))
    .pipe(filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe(flatten())
    .pipe(gulp.dest('.tmp/fonts'))
});
