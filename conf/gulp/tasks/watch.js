var gulp = require('gulp');
var sync = require('browser-sync');
var reload = sync.reload;


gulp.task('watch', ['clean', 'styles', 'scripts', 'images', 'fonts','templates', 'sync'], function() {

  gulp.watch('src/assets/fonts/**', ['fonts']);
  gulp.watch('src/assets/images/**', ['images']);
  gulp.watch([
    'src/assets/scripts/**',
    'gulpfile.js',
    'gulp/{**/,}*.js'], ['scripts']);
  gulp.watch('src/assets/styles/**', ['styles']);
  gulp.watch('src/templates/**', ['templates']);
  gulp.watch('bower.json', ['dependencies', 'fonts']);
  gulp.watch('.tmp/*', reload);
});
