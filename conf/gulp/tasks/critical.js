var gulp = require('gulp');
var critical = require('critical');

gulp.task('critical', ['build'], function () {
  critical.generateInline({
    base: 'dist/',
    src: 'index.html',
    styleTarget: 'styles/critical.css',
    htmlTarget: 'index.html',
    width: 320,
    height: 480,
    minify: true
  });
});
