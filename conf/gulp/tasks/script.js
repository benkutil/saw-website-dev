var gulp = require('gulp');
var jshint = require('gulp-jshint');
var size = require('gulp-size');


gulp.task('scripts', function () {
  return gulp.src(['src/assets/scripts/**/*.js', 'gulpfile.js'])
      .pipe(jshint())
      .pipe(jshint.reporter(require('jshint-stylish')))
      .pipe(size({gzip: true}))
      .pipe(gulp.dest('./.tmp/scripts'))
});
