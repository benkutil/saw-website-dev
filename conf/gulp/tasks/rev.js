var gulp = require('gulp');
var gif = require('gulp-if');
var minify = require('gulp-minify-html');
var MINIFY_OPTIONS = {comments:true,spare:true};
var revAll = require('gulp-rev-all');


gulp.task('rev', function () {
  return gulp.src(['.tmp/**', ])
    .pipe(revAll({
      base: ['.tmp', 'bower_components'],
      ignore: [
        /^\/favicon.ico$/g,
        '.html',
        '.php'
      ],
    }))
    .pipe(gif('*.html', minify(MINIFY_OPTIONS)))
    .pipe(gulp.dest('dist'));
});
