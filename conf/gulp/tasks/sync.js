var browserSync = require('browser-sync');
var gulp = require('gulp');

gulp.task('sync', function() {
  browserSync({
    server: {
      notify: false,
      baseDir: ['.tmp'],
      directory: false,
      index: "index.html",
      routes: {
          "/bower_components": "bower_components"
      }
    }
  });
});

gulp.task('sync:dist', function() {
  browserSync({
    server: {
      notify: false,
      baseDir: ['dist'],
      directory: false,
      index: "index.html",
      routes: {
          "/bower_components": "bower_components"
      }
    }
  });
});
