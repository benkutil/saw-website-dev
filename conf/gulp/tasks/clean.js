var gulp = require('gulp');
var del = require('del');

gulp.task('clean', ['clean:styles', 'clean:templates']);

gulp.task('clean:styles', function(done) {
  del('./.tmp/styles/*', done);
});

gulp.task('clean:templates', function(done) {
  del('./.tmp/**/*.html', done);
});
