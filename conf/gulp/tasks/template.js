var assemble = require('assemble');
var extname = require('gulp-extname');
var gif = require('gulp-if');
var gulp = require('gulp');
var gulpAssemble = require('gulp-assemble');

var ASSEMBLE_OPTIONS = {
  layout: 'default',
  layouts: './src/templates/layouts/*.hbs',
  partials: './src/templates/includes/*.hbs'
};

assemble.layouts(['./src/templates/layouts/*.hbs']);
assemble.partials(['./src/templates/includes/*.hbs']);



gulp.task('templates', function() {
  gulp.src('src/templates/pages/*.hbs')
    .pipe(gulpAssemble(assemble, { layout: 'default' }))
    .pipe(extname())
    .pipe(gif('*.html', gulp.dest('.tmp/')))

});
