var gulp = require('gulp');
var wiredep = require('wiredep').stream;


gulp.task('dependencies', function () {

  gulp.src('src/assets/styles/*.scss')
    .pipe(wiredep({
      directory: 'bower_components',
      ignorePath: '../../..',
    }))
    .pipe(gulp.dest('.tmp/styles'));

  gulp.src('src/templates/layouts/*.hbs')
    .pipe(wiredep({
      directory: 'bower_components',
      ignorePath: '../../..',
      overrides: {
        "loadcss": {
          "main": "loadCSS.js"
        },
      }
    }))
    .pipe(gulp.dest('src/templates/layouts/'));
});
