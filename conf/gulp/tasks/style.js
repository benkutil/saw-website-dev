var gulp = require('gulp');
var autoprefixer = require('autoprefixer-core');
var browserSync = require('browser-sync');
var cmq = require('gulp-combine-media-queries');
var colorguard = require('gulp-colorguard');
var csslint = require('gulp-csslint');
var csso = require('gulp-csso');
var filter = require('gulp-filter');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var size = require('gulp-size');
var sourcemaps = require('gulp-sourcemaps');
var uncss = require('gulp-uncss');

var AUTOPREFIXER_BROWSERS = [
 'ie >= 8',
 'ie_mob >= 10',
 'ff >= 30',
 'chrome >= 34',
 'safari >= 7',
 'opera >= 23',
 'ios >= 7',
 'android >= 4.4',
 'bb >= 10'
];





/* ==========================================================================
   § Compile Sass
   ========================================================================== */
gulp.task('styles', function() {
  return gulp.src([
    './src/assets/styles/main.scss',
    './src/assets/styles/live.scss',
    './src/assets/styles/webfonts.scss'
  ])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', gutil.log))
    .pipe(sourcemaps.write('./map'))
    .pipe(csslint())
    .pipe(gulp.dest('./.tmp/styles'))
    .pipe(filter('**/*.css'))
    .pipe(browserSync.reload({stream: true}))
    .pipe(size({gzip: true}));
});


gulp.task('styles:colorguard', function() {
  return gulp.src('./.tmp/styles/*.css')
    .pipe(plumber())
    .pipe(colorguard());
});


gulp.task('styles:optimize', function () {
  return gulp.src('./.tmp/styles/*.css')
    .pipe(cmq({
          log: true
    }))
    // Concatenate And Minify Styles
    .pipe(uncss({
      html: [
        '.tmp/*.html',
      ],
      // CSS Selectors for UnCSS to ignore
      ignore: [
        // for mobile
        /video, embed, object, img, picture/,
        /img/,
        // application styles
        /.app-bar.open/,
        /.ibutton-*/,
        /#header/,

      ]
    }))
    .pipe(csso())
    .pipe(gulp.dest('dist/styles'))
    .pipe(size({title: 'Media Queries'}));
});
