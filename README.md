#Succeed at Work Corporate Website

## Setup

Make sure you have the following dependencies installed

- [Node](http://nodejs.org)
- [NPM](http://npmjs.com)
- [Bower](http://bower.io)
- [Gulp](https://www.npmjs.com/package/gulp) installed globally with `sudo npm install --global gulp`

After these items are installed, run:

`npm install && bower install`

## Developing

Once through the setup step, you can run the following command to begin watching the source files (in `src`) and serving them using [BrowserSync](http://browsersync.io). The files get rendered into `.tmp`.

`gulp watch`

## Build

To build files for deployment, you can run:

`gulp build`

This runs some additional optimizations

## Critical

To inline critical css, the site uses [Critical](https://www.npmjs.com/package/critical) from [Addy Osmani](http://addyosmani.com).

`gulp critical`

## Serving "built" files

After running `gulp build` or `gulp critical`, you can run `gulp sync:dist` to display your built files in the browser.
